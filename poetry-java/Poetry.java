import java.util.HashMap;
import java.util.Map;

public class Poetry {

	public String rhymeScheme(String[] poem) {
		for (int i = 0; i < poem.length; i++) {
			String poemTrim = poem[i].trim().toLowerCase();
			int cur = poemTrim.length() - 1;

			if (cur != -1) {
				while (!isVowel(poemTrim, cur))
					cur--;
				while (cur >= 0 && isVowel(poemTrim, cur))
					cur--;
				poem[i] = poemTrim.substring(cur + 1);
			} else {
				poem[i] = poemTrim;
			}
		}

		Map<String, Character> labelMap = new HashMap<String, Character>();
		labelMap.put("", ' ');
		StringBuilder scheme = new StringBuilder();
		int nextLabel = 0;

		for (int i = 0; i < poem.length; i++) {
			Character lbl = labelMap.get(poem[i]);
			if (lbl == null) {
				lbl = labelFor(nextLabel++);
				labelMap.put(poem[i], lbl);
			}
			scheme.append(lbl);
		}
		return scheme.toString();
	}

	boolean isVowel(String str, int idx) {
		char ch = str.charAt(idx);
		if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u')
			return true;
		if (ch != 'y')
			return false;
		return idx != str.length() - 1 && idx != 0
				&& str.charAt(idx - 1) != ' ';
	}

	char labelFor(int idx) {
		if (idx < 26)
			return (char) ('a' + idx);
		else
			return (char) ('A' + (idx - 26));
	}
}
